package com.example.entregable;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    RequestQueue requestQueue;
    EditText User, Password;
    Button btnIngresar;
    public static final String KEY_USERNAME="email";
    public static final String KEY_PASSWORD="password";

    private String username;
    private String password;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        User = findViewById(R.id.user);
        Password = findViewById(R.id.password);
        btnIngresar = findViewById(R.id.btnIngresar);
        requestQueue = Volley.newRequestQueue(this);

        btnIngresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String username = User.getText().toString().trim();
                String password = Password.getText().toString().trim();

                if(!username.isEmpty() || !password.isEmpty()){
                    Login(username, password);
                }else{
                    Toast.makeText(MainActivity.this, "El usuario y contraseña no coinciden", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    // Falta agregar la comparación con los input y la redirección a la siguiente pantalla
    /*
    public void Inicio_sesion(){
        username = User.getText().toString().trim();
        password = Password.getText().toString().trim();
        String url = "https://run.mocky.io/v3/b4adf3d6-ae22-4771-b5d5-63c477d0d6f6";
        StringRequest stringRequest = new StringRequest(
                Request.Method.GET,
                url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response.trim().equals("success")){
                            //Entrar();
                            Toast.makeText(MainActivity.this, "Logueo exitoso", Toast.LENGTH_LONG).show();
                        }else {
                            Toast.makeText(MainActivity.this, "El usuario y contraseña no coinciden", Toast.LENGTH_LONG).show();
                        }
                    }
                },
                new Response.ErrorListener(){
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this,error.toString(), Toast.LENGTH_LONG).show();
                    }
                }
        ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put(KEY_USERNAME,username);
                params.put(KEY_PASSWORD, password);
                return params;
            }
        };
        RequestQueue requestQueue =Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    public void Entrar(){
        Intent ir = new Intent(this, Segunda_vista.class);
        ir.addFlags(ir.FLAG_ACTIVITY_CLEAR_TASK | ir.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(ir);
    }*/

    private void Login(String email, String password){
        String url = "https://run.mocky.io/v3/b4adf3d6-ae22-4771-b5d5-63c477d0d6f6";
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            //String success = jsonObject.getString("success");
                            JSONArray jsonArray = jsonObject.getJSONArray("user");

                            //if(success.equals("1")){
                                for(int i=0; i<jsonArray.length();i++){
                                    JSONObject object = jsonArray.getJSONObject(i);

                                    String name = object.getString("name").trim();
                                    String email = object.getString("email").trim();

                                    Toast.makeText(MainActivity.this, "Logueo exitoso", Toast.LENGTH_SHORT ).show();
                                }
                            //}
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(MainActivity.this, "Error "+e.toString(), Toast.LENGTH_SHORT ).show();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(MainActivity.this, "Error "+error.toString(), Toast.LENGTH_SHORT ).show();
                    }
                })
        {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("email", email);
                params.put("password", password);
                return super.getParams();
            }
        };
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}